from __future__ import print_function

import sys
from operator import add
import  findspark


findspark.init()
findspark.init("C:/Users/Andreia/spark/spark-2.1.1-bin-hadoop2.7")

from pyspark.sql import SparkSession

def teste(x):    

    a = x.split(' ')
    linha =""
    coluna = ""
    for item in a:
      linha = linha+ str(int(item[0])+1)
      coluna= coluna + item[1]
    
    #return linha+coluna
    #return linha
    return coluna

def limpaEspaco(line):
    #x = x.replace(" ","")
    line.split(' ')
    return line

if __name__ == "__main__":

    # inicia o spark
    spark = SparkSession.builder.appName("PythonWordCount").getOrCreate()
    # caminho do arquivo de texto
    txtfile = "C:/Users/Andreia/Desktop/GIT guilherme/trabalhobigdata/txtTeste.txt"
    # ler o arquivo de textp
    lines = spark.read.text(txtfile).rdd.map(lambda r: r[0])
    '''
    counts = lines.flatMap(lambda x: x.split(' ')) \
                  .map(lambda x: (x, 1)) \
                  .reduceByKey(add)
    '''
    '''
    counts = lines.flatMap(limpaEspaco).map(lambda x: (teste(x), 1)).reduceByKey(add)
   '''
    #flamap
    #map
    #reducebykey 
    counts = lines.flatMap(lambda x: x.split('\n')).map(lambda x:(teste(x), 1)) .reduceByKey(add)
                  #.map(lambda x: (x.replace(" ",""), 1)) \

                 
    
    output = counts.collect()
    for (word, count) in output:
        print("%s: %i" % (word, count))

    spark.stop()
